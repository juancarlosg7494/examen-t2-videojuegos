using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RobotScene2 : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sr;
    public float velocity  = 5;
    public float jump = 20;
    public GameObject rightBullet;
    public GameObject leftBullet;

    public GameObject rightBulletC;
    public GameObject leftBulletC;
    
    private GameController _game;
    public float muerte =0;
    
    public float carga = 0f;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        
        _game = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (_game.GetLifes()<=0)
        {
            animator.SetInteger("Esttado",6);
            muerte += Time.deltaTime;
            if (muerte >= 1 )
            {
                SceneManager.LoadScene("Scene_2");
            }
        }else
        {
            animator.SetInteger("Esttado", 0);
            rb.velocity = new Vector2(0, rb.velocity.y); 

            if (Input.GetKey(KeyCode.RightArrow))
            {
                rb.velocity = new Vector2(velocity, rb.velocity.y); 
                sr.flipX = false;
                animator.SetInteger("Esttado",1); 
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                rb.velocity = new Vector2(-velocity, rb.velocity.y); 
                sr.flipX = true; 
                animator.SetInteger("Esttado",1); 
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                rb.AddForce(Vector2.up * jump, ForceMode2D.Impulse);
                
                animator.SetInteger("Esttado",2); 
            }   
            
            if (Input.GetKey(KeyCode.Z))
            {
                animator.SetInteger("Esttado",3); 
            } 
            if (Input.GetKey(KeyCode.X)){
                carga += Time.deltaTime;
                
                if (sr.color == Color.red)
                {
                    sr.color= Color.white;
                }else{
                    sr.color= Color.red;
                }
            }
            if (Input.GetKeyUp(KeyCode.X))
            {
                sr.color= Color.white;
                if (rb.velocity.x == 0)
                {
                    animator.SetInteger("Esttado",4); 
                }else
                {
                    animator.SetInteger("Esttado",5); 
                }
                if (carga < 3)
                {
                    var bullet = sr.flipX ? leftBullet : rightBullet;
                    var position = new Vector2(transform.position.x, transform.position.y);
                    var rotation = rightBullet.transform.rotation;
                    Instantiate(bullet, position, rotation);
                }else
                {
                    var bullet = sr.flipX ? leftBulletC : rightBulletC;
                    var position = new Vector2(transform.position.x, transform.position.y);
                    var rotation = rightBulletC.transform.rotation;
                    Instantiate(bullet, position, rotation);
                }
                carga=0;
                
                
            }
        }
        
        
    }
   
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            
            _game.LoseLife();
            
        }
        
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Llave"))
        {
            if (_game.GetEnemys() == 0)
            {
                SceneManager.LoadScene("Scene_3");
            }
            
        }
    }

}